
package com.my.power.network.parser;

import android.util.Log;

import com.my.power.MyPowerApplication;
import com.my.power.network.response.BaseResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Base parser class. Should parse basic response structure, e.g. server-side errors, response status code. Validates incoming JSON.
 *
 * @param <T> response model type
 */

public class BaseParser<T extends BaseResponse> implements Parser<T> {

	public static final String DATE_TIME_PATTERN = "yyyy-MM-dd\'T\'HH:mm:ss";
	public static final String DATE_PATTERN = "yyyy-MM-dd";

	protected JSONObject mRootJsonObject;
	protected JSONArray mRootJsonArray;

	protected T mResponse;

	@Override
	public T getResponse() {
		return mResponse;
	}

	@Override
	public void parse(String response) {
		try {
			mRootJsonObject = new JSONObject(response);
		} catch (JSONException e) {
			try {
				mRootJsonArray = new JSONArray(response);
			} catch (JSONException e1) {
				logInvalidResponseError();
			}
		}
	}

	protected boolean hasValidJsonObject() {
		return (mRootJsonObject != null);
	}

	protected boolean hasValidJsonArray() {
		return (mRootJsonArray != null);
	}

	/* logging */

	protected void logInvalidResponseError() {
		Log.e(getClass().getName(), "Parser error occured. Please check parser implementation.");
	}

	protected void logResponseWarning(Exception e) {
		Log.e(getClass().getName(),
				"Parser warning occured. Please check parser implementation. Exception = " + e.getLocalizedMessage());
	}

	/* help methods */
	protected Date getDateFromString(String stringDate, String pattern) {
		Date date = new Date();
		final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, 
				MyPowerApplication.getAppContext().getResources().getConfiguration().locale);
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {
			date = simpleDateFormat.parse(stringDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	protected String parceNullString(JSONObject jsonObject, String key) {
		String str = new String();
		try {
			str = jsonObject.getString(key);
		} catch (JSONException e) {
			return null;
		}
		if (str.equals("null")) {
			return null;
		}
		return str;
	}
}
