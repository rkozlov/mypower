
package com.my.power.network.parser;

import com.my.power.model.Program;
import com.my.power.network.WebService;
import com.my.power.network.response.AllProgramsResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class AllProgramsParser extends BaseParser<AllProgramsResponse> {

	@Override
	public void parse(String response) {
		mResponse = new AllProgramsResponse();
		super.parse(response);

        ArrayList<Program> programs = new ArrayList<Program>();

		if (!hasValidJsonObject()) {
			logInvalidResponseError();
		} else {
			int code = mRootJsonObject.optInt("success");
			mResponse.setCode(code);
			if (code == WebService.CODE_OK) {
				JSONArray resultArray = mRootJsonObject.optJSONArray("object");
				if (resultArray != null) {
					for (int index = 0; index < resultArray.length(); index++) {
                        Program program = new Program();
                        JSONObject programJsonObject = resultArray.optJSONObject(index);

                        program.setName(programJsonObject.optString("nameOfProgram"));
                        program.setId(programJsonObject.optString("id"));
                        program.setAuthor(programJsonObject.optString("author"));
                        program.setDescription(programJsonObject.optString("description"));
                        program.setImage(programJsonObject.optString("image"));

                        programs.add(program);
					}
				}
			} else {
				mResponse.setMessage(mRootJsonObject.optString("message"));
			}
            mResponse.setPrograms(programs);
		}

	}
}

