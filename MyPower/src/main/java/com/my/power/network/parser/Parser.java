
package com.my.power.network.parser;

/**
 * Generic parser interfaces. Should implement parse method and be able to return response.
 */

public interface Parser<T> {
    void parse(String response);

    T getResponse();
}
