package com.my.power.network.volley;

import com.android.volley.AuthFailureError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class CustomJsonObjectRequest extends JsonObjectRequest {
	
	private String authToken;

	public CustomJsonObjectRequest(int method, String url, JSONObject jsonRequest,
			Listener<JSONObject> listener, ErrorListener errorListener, String authToken) {
		super(method, url, jsonRequest, listener, errorListener);
		setShouldCache(false);
		this.authToken = authToken;
	}
	
	public CustomJsonObjectRequest(int method, String url, JSONObject jsonRequest,
			Listener<JSONObject> listener, ErrorListener errorListener) {
		super(method, url, jsonRequest, listener, errorListener);
		setShouldCache(false);
	}

	public CustomJsonObjectRequest(String url, JSONObject jsonRequest,
			Listener<JSONObject> listener, ErrorListener errorListener) {
		super(url, jsonRequest, listener, errorListener);
		setShouldCache(false);
	}

	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {
		Map<String, String> headerMap = new HashMap<String, String>();
		//TODO: remove hardcode
		//			headerMap.put("Accept-Language", locale);
		headerMap.put("Accept-Language", "ru");
		if (authToken != null) {
			headerMap.put("auth_token", authToken);
		}

		return headerMap;
	}
}
