
package com.my.power.network.parser;

import com.my.power.MyPowerApplication;
import com.my.power.R;
import com.my.power.db.dao.DayTableDao;
import com.my.power.db.dao.ExerciseTableDao;
import com.my.power.db.dao.WeekTableDao;
import com.my.power.model.Day;
import com.my.power.model.Exercise;
import com.my.power.model.Program;
import com.my.power.model.Week;
import com.my.power.network.WebService;
import com.my.power.network.response.ProgramResponse;

import org.json.JSONArray;
import org.json.JSONObject;

public class ProgramParser extends BaseParser<ProgramResponse> {

    @Override
    public void parse(String response) {
        mResponse = new ProgramResponse();
        super.parse(response);

        Program program = new Program();

        if (!hasValidJsonObject()) {
            logInvalidResponseError();
        } else {
            int code = mRootJsonObject.optInt("success");
            mResponse.setCode(code);
            if (code == WebService.CODE_OK) {
                String weekName = MyPowerApplication.getResourceString(R.string.program_info_week);
                String dayName = MyPowerApplication.getResourceString(R.string.program_info_day);

                JSONObject resultObject = mRootJsonObject.optJSONObject("object");
                program.setId(resultObject.optString("id"));
                program.setImage(resultObject.optString("image"));
                program.setName(resultObject.optString("nameOfProgram"));
                program.setAuthor(resultObject.optString("author"));
                program.setDescription(resultObject.optString("description"));

                WeekTableDao weekTableDao = new WeekTableDao(MyPowerApplication.getAppContext());
                DayTableDao dayTableDao = new DayTableDao(MyPowerApplication.getAppContext());
                ExerciseTableDao exerciseTableDao = new ExerciseTableDao(MyPowerApplication.getAppContext());

                JSONArray weeksArray = resultObject.optJSONArray("program");
                for (int j = 0; j < weeksArray.length(); j++) {
                    Week week = new Week();
                    week.setProgram(program);
                    JSONObject daysObject = weeksArray.optJSONObject(j);
                    JSONArray daysArray = daysObject.optJSONArray("week");
                    week.setName(weekName + " " + daysObject.optString("numberOfWeek"));
                    weekTableDao.createOrUpdate(week);
                    for (int k = 0; k < daysArray.length(); k++) {
                        Day day = new Day();
                        day.setWeek(week);
                        JSONObject exercisesObject = daysArray.optJSONObject(k);
                        JSONArray exercisesArray = exercisesObject.optJSONArray("day");
                        day.setName(dayName + " " + exercisesObject.optString("numberOfDay"));
                        dayTableDao.createOrUpdate(day);
                        for (int l = 0; l < exercisesArray.length(); l++) {
                            Exercise exercise = new Exercise();
                            exercise.setDay(day);
                            JSONObject exerciseObject = exercisesArray.optJSONObject(l);
                            exercise.setIterations(exerciseObject.optInt("iteration"));
                            exercise.setWeight((float) exerciseObject.optDouble("weight"));
                            exercise.setTimes(exerciseObject.optInt("times"));
                            exercise.setName(exerciseObject.optString("name"));

                            exerciseTableDao.createOrUpdate(exercise);
                        }
                    }
                }
            } else {
                mResponse.setMessage(mRootJsonObject.optString("message"));
            }
        }
        mResponse.setProgram(program);
    }
}

