
package com.my.power.network;

/**
 * ResponseReceiver is universal method to handle async network operations result.
 *
 * Generic parameter is used for type-safe pass of response model. 
 */

public abstract class ResponseReceiver<T> {

    public abstract void onSuccess(T response);

    public void onFailure() {
        // override this method to manually handle error
    }
}
