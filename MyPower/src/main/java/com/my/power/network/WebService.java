package com.my.power.network;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.my.power.MyPowerApplication;
import com.my.power.R;
import com.my.power.network.parser.AllProgramsParser;
import com.my.power.network.parser.BaseParser;
import com.my.power.network.parser.ProgramParser;
import com.my.power.network.response.AllProgramsResponse;
import com.my.power.network.response.BaseResponse;
import com.my.power.network.response.ProgramResponse;
import com.my.power.network.volley.CustomJsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by user on 11.05.14.
 */
public class WebService {

    public static final String LOG_TAG = "WebService";
    public static final int CODE_OK = 0;

    private static WebService instance;

    private Activity mActivity;
    private ProgressDialog mProgressDialog;

    private RequestQueue mRequestQueue;

    private WebService() {
        mRequestQueue = Volley.newRequestQueue(MyPowerApplication.getAppContext());
    }

    public static WebService getInstance() {
        if (instance == null) {
            instance = new WebService();
        }
        return instance;
    }

    public void setActivity(Activity mActivity) {
        this.mActivity = mActivity;
    }

    /**
     * Display progress dialog alert on UI context (if set)
     */
    private void showLoadingIndicator() {
        if (mActivity != null) {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(mActivity);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage(mActivity.getString(R.string.loading));
            }
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        }
    }

    /**
     * Dismisses progress dialog alert on UI context (if set)
     */
    public void dismissLoadingIndicator() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    /**
     * Display network error alert with retry button on UI context (if set)
     *
     * @param retryAction runnable to run when tryAgain button tapped
     * @param cancelAction runnable to run when cancel button taped
     */
    private void showRetryAlert(final Runnable retryAction, final Runnable cancelAction) {
        if (mActivity != null) {
            new AlertDialog.Builder(mActivity).setTitle(R.string.error).setMessage(R.string.network_error_occured)
                    .setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (retryAction != null) {
                                retryAction.run();
                            }
                        }
                    }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (cancelAction != null) {
                        cancelAction.run();
                    }
                }
            }).create().show();
        }
    }

    /* Base Methods */
    /**
     * HTTP POST request method
     *
     * @param url request URL
     * @param parser response parcer
     * @param receiver to handle response
     */
    private <T extends BaseResponse> void requestPost(final String url, final JSONObject paramsJsonObject,
                                                      final BaseParser<T> parser, final ResponseReceiver<T> receiver) {
        Response.Listener<JSONObject> responseListener = createResponseListener(url,
                paramsJsonObject, parser, receiver, Request.Method.POST);
        Response.ErrorListener errorListener = getErrorListener(Request.Method.POST, url,
                paramsJsonObject, parser, receiver);

        CustomJsonObjectRequest jsonObjectRequest = new CustomJsonObjectRequest(Request.Method.POST,
                    url, paramsJsonObject, responseListener, errorListener);
        mRequestQueue.add(jsonObjectRequest);
    }

    /**
     * HTTP GET request method
     *
     * @param url request URL
     * @param parser response parcer
     * @param receiver to handle response
     */
    private <T extends BaseResponse> void requestGet(final String url,
                                                     final BaseParser<T> parser, final ResponseReceiver<T> receiver) {
        Response.Listener<JSONObject> responseListener = createResponseListener(url, null, parser, receiver, Request.Method.GET);
        Response.ErrorListener errorListener = getErrorListener(Request.Method.GET, url,
                parser, receiver);

        CustomJsonObjectRequest jsonObjectRequest = new CustomJsonObjectRequest(Request.Method.GET,
                url, null, responseListener, errorListener, null);

        mRequestQueue.add(jsonObjectRequest);
    }

    /**
     * HTTP DELETE request method
     *
     * @param url request URL
     * @param parser response parcer
     * @param receiver to handle response
     */
    private <T extends BaseResponse> void requestDelete(final String url,
                                                        final BaseParser<T> parser, final ResponseReceiver<T> receiver) {
        showLoadingIndicator();
        mRequestQueue.getCache().clear();

        Response.Listener<JSONObject> responseListener = createResponseListener(url, null, parser, receiver, Request.Method.DELETE);
        Response.ErrorListener errorListener = getErrorListener(Request.Method.DELETE, url,
                parser, receiver);

        CustomJsonObjectRequest jsonObjectRequest = new CustomJsonObjectRequest(Request.Method.DELETE,
                url, null, responseListener, errorListener);

        mRequestQueue.add(jsonObjectRequest);
    }

    /**
     * HTTP PUT request method
     *
     * @param url request URL
     * @param parser response parcer
     * @param receiver to handle response
     */
    private <T extends BaseResponse> void requestPut(final String url, final JSONObject paramsJsonObject,
                                                     final BaseParser<T> parser, final ResponseReceiver<T> receiver) {
        showLoadingIndicator();

        Response.Listener<JSONObject> responseListener = createResponseListener(url, null, parser, receiver, Request.Method.PUT);
        Response.ErrorListener errorListener = getErrorListener(Request.Method.PUT, url,
                paramsJsonObject, parser, receiver);

        CustomJsonObjectRequest jsonObjectRequest = new CustomJsonObjectRequest(Request.Method.PUT,
                url, paramsJsonObject, responseListener, errorListener);

        mRequestQueue.add(jsonObjectRequest);
    }

    /* Help methods */
    private <T extends BaseResponse> Response.Listener<JSONObject> createResponseListener(
            final String url, final JSONObject paramsJsonObject,
            final BaseParser<T> parser, final ResponseReceiver<T> receiver, final int requestType) {
        Log.d(LOG_TAG, ("request url = " + url == null ? "null" : url + " params = ") + (paramsJsonObject == null ? "null" : paramsJsonObject.toString()));
        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dismissLoadingIndicator();

                String responseString = response.toString();
                Log.i(LOG_TAG, responseString);
                parser.parse(responseString);
                receiver.onSuccess(parser.getResponse());
            }
        };
        return responseListener;
    }

    private <T extends BaseResponse> Response.ErrorListener getErrorListener(final int requestType,
                                                                             final String url, final BaseParser<T> parser, final ResponseReceiver<T> receiver) {
        return getErrorListener(requestType, url, null, parser, receiver);
    }

    private <T extends BaseResponse> Response.ErrorListener getErrorListener(final int requestType,
                                                                             final String url, final JSONObject paramsJsonObject,
                                                                             final BaseParser<T> parser, final ResponseReceiver<T> receiver) {
        Response.ErrorListener errorListener = new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i(LOG_TAG, error.toString());
                dismissLoadingIndicator();

//                showRetryAlert(new Runnable() {
//                    @Override
//                    public void run() {
//                        showLoadingIndicator();
//                        switch (requestType) {
//                            case Request.Method.POST:
//                                requestPost(url, paramsJsonObject, parser, receiver);
//                                break;
//                            case Request.Method.DELETE:
//                                requestDelete(url, parser, receiver);
//                                break;
//                            case Request.Method.GET:
//                                requestGet(url, parser, receiver);
//                                break;
//                            case Request.Method.PUT:
//                                requestPut(url, paramsJsonObject,parser, receiver);
//                                break;
//                            default:
//                                break;
//                        }
//                    }
//                }, new Runnable() {
//                    @Override
//                    public void run() {
//                        receiver.onFailure();
//                    }
//                });
                receiver.onFailure();
            }
        };

        return errorListener;
    }

    //Derivative methods

    public void requestAllPrograms(ResponseReceiver<AllProgramsResponse> responseReceiver) {
        requestGet(ConstantsNetwork.BASE_URL + ConstantsNetwork.ALL_PROGRAMS,
                new AllProgramsParser(), responseReceiver);
    }

    public void requestProgram(String id, ResponseReceiver<ProgramResponse> responseReceiver) {
//        showLoadingIndicator();
        JSONObject params = new JSONObject();
        try {
            params.put("id", id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        requestPost(ConstantsNetwork.BASE_URL + ConstantsNetwork.GET_PROGRAM + "?id=" + id, params,
                new ProgramParser(), responseReceiver);
    }
}
