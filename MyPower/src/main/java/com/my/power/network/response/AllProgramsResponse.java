package com.my.power.network.response;

import com.my.power.model.Program;

import java.util.ArrayList;

/**
 * Created by user on 11.05.14.
 */
public class AllProgramsResponse extends  BaseResponse {

    ArrayList<Program> programs;

    public ArrayList<Program> getPrograms() {
        return programs;
    }

    public void setPrograms(ArrayList<Program> programs) {
        this.programs = programs;
    }
}
