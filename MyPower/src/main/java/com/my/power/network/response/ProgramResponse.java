package com.my.power.network.response;

import com.my.power.model.Program;

/**
 * Created by user on 11.05.14.
 */
public class ProgramResponse extends  BaseResponse {

    Program program;

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }
}
