package com.my.power.network.response;

/**
 * Base web service API response model, containing response error code 
 */

public class BaseResponse {

	protected int code;
	protected String message;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
