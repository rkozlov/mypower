package com.my.power.image;

import android.content.Context;

import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by user on 11.05.14.
 */
public class CustomImageDownloader extends BaseImageDownloader {

    public CustomImageDownloader(Context context) {
        super(context);
    }

    @Override
    protected InputStream getStreamFromOtherSource(String imageUri, Object extra) throws IOException {
        return super.getStreamFromOtherSource(imageUri, extra);
    }
}
