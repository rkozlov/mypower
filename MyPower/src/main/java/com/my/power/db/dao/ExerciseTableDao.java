package com.my.power.db.dao;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;
import com.my.power.db.DatabaseHelper;
import com.my.power.db.DatabaseManager;
import com.my.power.db.dao.interfaces.DaoInterface;
import com.my.power.model.Exercise;

import java.sql.SQLException;
import java.util.List;

/**
 * 
 * This class provides the access to the database data from table named Country
 *
 */
public class ExerciseTableDao implements DaoInterface<Exercise> {
    private DatabaseHelper mDatabase;
    Dao<Exercise, Integer> mExercisesDao;

    public ExerciseTableDao(Context context) {
        try {
            DatabaseManager dbManager = new DatabaseManager();
            mDatabase = dbManager.getHelper(context);
            mExercisesDao = mDatabase.getExercisesDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int create(Exercise exercise)  {
        try {
            return mExercisesDao.create(exercise);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
	public CreateOrUpdateStatus createOrUpdate(Exercise exercise) {
    	try {
            return mExercisesDao.createOrUpdate(exercise);
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return null;
	}

    @Override
    public int update(Exercise exercise) {
        try {
            return mExercisesDao.update(exercise);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delete(Exercise exercise) {
        try {
            return mExercisesDao.delete(exercise);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Exercise> getAll() {
        try {
            return mExercisesDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean refresh(Exercise exercise) {
        try {
            mExercisesDao.refresh(exercise);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  false;
    }
}
