package com.my.power.db.dao;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;
import com.my.power.db.DatabaseHelper;
import com.my.power.db.DatabaseManager;
import com.my.power.db.dao.interfaces.DaoInterface;
import com.my.power.model.Program;

import java.sql.SQLException;
import java.util.List;

/**
 * 
 * This class provides the access to the database data from table named Country
 *
 */
public class ProgramTableDao implements DaoInterface<Program> {
    private DatabaseHelper mDatabase;
    Dao<Program, String> mProgramsDao;
 
    public ProgramTableDao(Context context) {
        try {
            DatabaseManager dbManager = new DatabaseManager();
            mDatabase = dbManager.getHelper(context);
            mProgramsDao = mDatabase.getProgramsDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
 
    @Override
    public int create(Program program)  {
        try {
            return mProgramsDao.create(program);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    @Override
	public CreateOrUpdateStatus createOrUpdate(Program tableField) {
    	try {
            return mProgramsDao.createOrUpdate(tableField);
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return null;
	}

    public boolean refresh(Program program) {
        try {
            mProgramsDao.refresh(program);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  false;
    }

    @Override
    public int update(Program program) {
        try {
            return mProgramsDao.update(program);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    @Override
    public int delete(Program program) {
        try {
            return mProgramsDao.delete(program);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
 
    /**
     * This method return all elements from the Country table
     * @return <code>List<Program></code> with all elements from {@link Program} table
     */
    public List<Program> getAll() {
        try {
            return mProgramsDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
