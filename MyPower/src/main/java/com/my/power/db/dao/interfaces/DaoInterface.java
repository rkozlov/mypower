package com.my.power.db.dao.interfaces;

import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;
import com.my.power.model.BaseDbModel;

/**
 * 
 * This is the interfaces with generic parameter that must extends {@link BaseDbModel}.
 * It contains methods for deleting and upgrading data in database that can be used for
 * different Database Model 
 *  
 * @param <T> is the generic parameter that extends the BaseDbModel
 */
public interface DaoInterface <T extends BaseDbModel> {
	int create(T tableField);
	
	CreateOrUpdateStatus createOrUpdate(T tableField);
	
	/**
	 * This method allows to update the data in database
	 * @param tableField specifies the field that should be updated
	 * @return 1 in case of successful updating and 0 in case of failure
	 */
	int update(T tableField);
	
	/**
	 *  This method allows to delete the data in database
	 * @param tableField specifies the field that should be deleted
	 * @return 1 in case of successful deleting and 0 in case of failure
	 */
	int delete(T tableField);
}
