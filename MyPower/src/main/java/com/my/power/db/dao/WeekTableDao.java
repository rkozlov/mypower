package com.my.power.db.dao;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;
import com.my.power.db.DatabaseHelper;
import com.my.power.db.DatabaseManager;
import com.my.power.db.dao.interfaces.DaoInterface;
import com.my.power.model.Week;

import java.sql.SQLException;
import java.util.List;

/**
 * 
 * This class provides the access to the database data from table named Country
 *
 */
public class WeekTableDao implements DaoInterface<Week> {
    private DatabaseHelper mDatabase;
    Dao<Week, Integer> mWeeksDao;

    public WeekTableDao(Context context) {
        try {
            DatabaseManager dbManager = new DatabaseManager();
            mDatabase = dbManager.getHelper(context);
            mWeeksDao = mDatabase.getWeeksDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int create(Week week)  {
        try {
            return mWeeksDao.create(week);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
	public CreateOrUpdateStatus createOrUpdate(Week tableField) {
    	try {
            return mWeeksDao.createOrUpdate(tableField);
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return null;
	}

    @Override
    public int update(Week week) {
        try {
            return mWeeksDao.update(week);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delete(Week week) {
        try {
            return mWeeksDao.delete(week);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Week> getAll() {
        try {
            return mWeeksDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean refresh(Week week) {
        try {
            mWeeksDao.refresh(week);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  false;
    }
}
