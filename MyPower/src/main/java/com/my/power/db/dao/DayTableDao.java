package com.my.power.db.dao;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;
import com.my.power.db.DatabaseHelper;
import com.my.power.db.DatabaseManager;
import com.my.power.db.dao.interfaces.DaoInterface;
import com.my.power.model.Day;

import java.sql.SQLException;
import java.util.List;

/**
 * 
 * This class provides the access to the database data from table named Country
 *
 */
public class DayTableDao implements DaoInterface<Day> {
    private DatabaseHelper mDatabase;
    Dao<Day, Integer> mDaysDao;

    public DayTableDao(Context context) {
        try {
            DatabaseManager dbManager = new DatabaseManager();
            mDatabase = dbManager.getHelper(context);
            mDaysDao = mDatabase.getDaysDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int create(Day day)  {
        try {
            return mDaysDao.create(day);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
	public CreateOrUpdateStatus createOrUpdate(Day day) {
    	try {
            return mDaysDao.createOrUpdate(day);
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return null;
	}

    @Override
    public int update(Day day) {
        try {
            return mDaysDao.update(day);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delete(Day day) {
        try {
            return mDaysDao.delete(day);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Day> getAll() {
        try {
            return mDaysDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean refresh(Day day) {
        try {
            mDaysDao.refresh(day);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  false;
    }
}
