package com.my.power.db;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

/**
 * 
 * This class provides the once database creation and ensures it doesn't create anew one
 *
 */
public class DatabaseManager {
	
    private DatabaseHelper databaseHelper = null;
    
    // gets a helper once one is created ensures it doesn't create a new one
    public DatabaseHelper getHelper(Context context) {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        }
        return databaseHelper;
    }
 
    // releases the helper once usages has ended
    public void releaseHelper(DatabaseHelper helper) {
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
