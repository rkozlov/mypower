package com.my.power.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.my.power.model.Day;
import com.my.power.model.Exercise;
import com.my.power.model.Program;
import com.my.power.model.User;
import com.my.power.model.Week;

import java.sql.SQLException;

/**  
 * This class used to manage the creation and upgrading of  
 * database. This class also provides the Data Access Objects used by the other  
 * classes.  
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

	private static final String DATABASE_NAME = "mypower_database.db";
	private static final int DATABASE_VERSION = 1;

    private Dao<Program, String> mProgramsDao = null;
    private Dao<Week, Integer> mWeeksDao = null;
    private Dao<Day, Integer> mDaysDao = null;
    private Dao<Exercise, Integer> mExercisesDao = null;
    private Dao<User, String> mUserDao = null;

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	/**  
	 * This method calls when the database is first created. It is necessary 
	 * to call createTable statements here to create the tables that will store  
	 * the data.  
	 */ 
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Program.class);
            TableUtils.createTable(connectionSource, Week.class);
            TableUtils.createTable(connectionSource, Day.class);
            TableUtils.createTable(connectionSource, Exercise.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	/**  
	 * This method calls when the application is upgraded and it has a higher  
	 * version number. This allows to adjust the various data to match the  
	 * new version number.  
	 */ 
	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion,
			int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Program.class, true);
            TableUtils.dropTable(connectionSource, Week.class, true);
            TableUtils.dropTable(connectionSource, Day.class, true);
            TableUtils.dropTable(connectionSource, Exercise.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}

    public Dao<Program, String> getProgramsDao() throws SQLException {
        if (mProgramsDao == null) {
            mProgramsDao = getDao(Program.class);
        }
        return mProgramsDao;
    }

    public Dao<Week, Integer> getWeeksDao() throws SQLException {
        if (mWeeksDao == null) {
            mWeeksDao = getDao(Week.class);
        }
        return mWeeksDao;
    }

    public Dao<Day, Integer> getDaysDao() throws SQLException {
        if (mDaysDao == null) {
            mDaysDao = getDao(Day.class);
        }
        return mDaysDao;
    }

    public Dao<Exercise, Integer> getExercisesDao() throws SQLException {
        if (mExercisesDao == null) {
            mExercisesDao = getDao(Exercise.class);
        }
        return mExercisesDao;
    }

    public Dao<User, String> getUserDao() throws  SQLException {
        if (mUserDao == null) {
            mUserDao = getDao(User.class);
        }
        return  mUserDao;
    }
	/**
	 * Close the database connections and clear any cached Database Access Objects (DAOs). 
	 */
	@Override
	public void close() {
		super.close();

	}

}
