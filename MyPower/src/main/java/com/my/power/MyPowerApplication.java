package com.my.power;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.my.power.image.CustomImageDownloader;
import com.nostra13.universalimageloader.cache.disc.impl.TotalSizeLimitedDiscCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by user on 29.03.14.
 */
public class MyPowerApplication extends Application {
    // if i am in need of it, i will add smth here

    private static final int DISC_CACHE_SIZE_BYTES = 5 * 1024 * 1024; // 5 MB
    private static final int MEMORY_CACHE_SIZE_BYTES = 2 * 1024 * 1024; // 2 MB

    private static Context sContext;
    private static int sDeviceScreenWidth;
    private static int sDeviceScreenHeight;
    private static int sStatusBarHeight;
    private static float sDeviceScreenDensity;
    private static boolean sIsTablet;

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {
        super.onCreate();

        sContext = getApplicationContext();

        WindowManager wm = (WindowManager) sContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR2) {
            sDeviceScreenWidth = display.getWidth();
            sDeviceScreenHeight = display.getHeight();
        } else {
            Point size = new Point();
            display.getSize(size);
            sDeviceScreenWidth = size.x;
            sDeviceScreenHeight = size.y;
        }

        if (sDeviceScreenWidth > sDeviceScreenHeight) {
            // launched in landscape, just swap
            int temp = sDeviceScreenWidth;
            sDeviceScreenWidth = sDeviceScreenHeight + 70; // TODO: remove hardcode (nexus bottom buttons panel height)
            sDeviceScreenHeight = temp;
        }

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        sDeviceScreenDensity = metrics.density;

        boolean isHoneycomb = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
        boolean isTablet = (sContext.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
        sIsTablet = isHoneycomb && isTablet;

        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            sStatusBarHeight = getResources().getDimensionPixelSize(resourceId);
        }

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB) {
            executeAsyncTask();
        }

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .memoryCache(new LruMemoryCache(MEMORY_CACHE_SIZE_BYTES))
                .imageDownloader(new CustomImageDownloader(sContext))
                .discCache(new TotalSizeLimitedDiscCache(getCacheDir(), DISC_CACHE_SIZE_BYTES)).build();

        ImageLoader.getInstance().init(config);
    }

	/* Global helper methods to work with application resources */


    /**
     * Shared applicaion context
     *
     * @return Safe context
     */
    public static Context getAppContext() {
        return sContext;
    }

    /**
     * Obtains string with resource ID
     *
     * @param resId String resource identifier
     */
    public static String getResourceString(int resId) {
        return sContext.getResources().getString(resId);
    }

    /**
     * Obtains color with resource ID
     *
     * @param resId Color resource identifier
     */
    public static int getResourceColor(int resId) {
        return sContext.getResources().getColor(resId);
    }

    /**
     * Obtains dimens with resource ID
     *
     * @param resId Dimen resource identifier
     */
    public static int getResourceDimensionPixelSize(int resId) {
        return sContext.getResources().getDimensionPixelSize(resId);
    }

    /**
     * Obtains bitmap with resource ID
     *
     * @param resId Bitmap resource identifier
     * @return bitmap
     */
    public static Bitmap getResourceBitmap(int resId) {
        return BitmapFactory.decodeResource(sContext.getResources(), resId);
    }

    /**
     * Obtains drawable with resource ID
     *
     * @param resId Drawable resource identifier
     * @return drawable
     */
    public static Drawable getResourcesDrawable(int resId) {
        return sContext.getResources().getDrawable(resId);
    }

    /**
     * Inflates XML resource
     *
     * @param resId XML resource identifier
     * @return inflated view
     */
    public static View getInflatedView(int resId) {
        return View.inflate(sContext, resId, null);
    }

    /**
     * @return Device screen width
     */
    public static int getDeviceWidth() {
        return sDeviceScreenWidth;
    }

    /**
     * @return Device screen height
     */
    public static int getDeviceHeight() {
        return sDeviceScreenHeight;
    }

    /**
     * @return Device screen density
     */
    public static float getDeviceDensity() {
        return sDeviceScreenDensity;
    }

    /**
     * Detects is device tablet. Not best working, but Google recommended way.
     * See https://code.google.com/p/iosched/, UIUtils class for details
     */

    public static boolean isTablet() {
        return false;
        //TODO: uncomment
        //		return sIsTablet;
    }

    /**
     * method, which helps
     * to use AsyncTask class
     * in versions on Android OS lower
     * then API 11
     */
    public static void executeAsyncTask() {
        Looper looper = Looper.getMainLooper();
        Handler handler = new Handler(looper);
        handler.post(new Runnable() {
            public void run() {
                try {
                    Class.forName("android.os.AsyncTask");
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static int getStatusBarHeight() {
        return sStatusBarHeight;
    }

    public static void hideKeyBoard(View view) {
        InputMethodManager imm = (InputMethodManager) sContext.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
