package com.my.power.model;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by user on 09.05.14.
 */
public class BaseProgramInfo extends  BaseDbModel {

    @DatabaseField
    protected String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
