package com.my.power.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by cool04ek on 15.02.14.
 */
@DatabaseTable(tableName = "exercise_table")
public class Exercise extends BaseProgramInfo implements Serializable {

    static final long serialVersionUID = 4L;

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign=true, foreignAutoRefresh=true)
    private Day day;

    @DatabaseField
    private int times;
    @DatabaseField
    private int iterations;
    @DatabaseField
    private float weight;
    @DatabaseField
    private boolean done;

    public Exercise() {
    }

    public Exercise(String name, int times, int iterations, float weight) {
        this.name = name;
        this.times = times;
        this.iterations = iterations;
        this.weight = weight;
    }

    public int getTimes() {
        return times;
    }

    public void setTimes(int times) {
        this.times = times;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public int getId() {
        return id;
    }
}
