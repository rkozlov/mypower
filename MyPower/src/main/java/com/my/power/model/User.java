package com.my.power.model;

import java.io.Serializable;

/**
 * Created by user on 12.06.14.
 */
public class User implements Serializable {

    public final static float NO_WEIGHT = 0F;
    public final static int NO_PRESS = 0;

    static final long serialVersionUID = 1L;

    private String id;

    private Program program;

    private float weight;

    private int benchPress;

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getBenchPress() {
        return benchPress;
    }

    public void setBenchPress(int benchPress) {
        this.benchPress = benchPress;
    }
}
