package com.my.power.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by cool04ek on 15.02.14.
 */
@DatabaseTable(tableName = "day_table")
public class Day extends BaseProgramInfo implements Serializable {

    static final long serialVersionUID = 3L;

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(foreign=true, foreignAutoRefresh=true)
    private Week week;

    @ForeignCollectionField(eager = true)
    private ForeignCollection<Exercise> exerciseForeignCollection;

//    ArrayList<Exercise> exercises;

//    public ArrayList<Exercise> getExercises() {
//        return exercises;
//    }
//
//    public void setExercises(ArrayList<Exercise> exercises) {
//        this.exercises = exercises;
//    }

    public ForeignCollection<Exercise> getExerciseForeignCollection() {
        return exerciseForeignCollection;
    }

    public void setExerciseForeignCollection(ForeignCollection<Exercise> exerciseForeignCollection) {
        this.exerciseForeignCollection = exerciseForeignCollection;
    }

    public Week getWeek() {
        return week;
    }

    public void setWeek(Week week) {
        this.week = week;
    }

    public int getId() {
        return id;
    }
}
