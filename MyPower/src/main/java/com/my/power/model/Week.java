package com.my.power.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by cool04ek on 15.02.14.
 */
@DatabaseTable(tableName = "weeks_table")
public class Week extends BaseProgramInfo implements Serializable {

    static final long serialVersionUID = 2L;

    @DatabaseField(foreign=true, foreignAutoRefresh=true)
    private Program program;

    @DatabaseField(generatedId = true)
    private int id;

    @ForeignCollectionField(eager = true)
    private ForeignCollection<Day> dayForeignCollection;

//    private ArrayList<Day> days;

//    public ArrayList<Day> getDays() {
//        return days;
//    }
//
//    public void setDays(ArrayList<Day> days) {
//        this.days = days;
//    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public ForeignCollection<Day> getDayForeignCollection() {
        return dayForeignCollection;
    }

    public void setDayForeignCollection(ForeignCollection<Day> dayForeignCollection) {
        this.dayForeignCollection = dayForeignCollection;
    }

    public int getId() {
        return id;
    }
}
