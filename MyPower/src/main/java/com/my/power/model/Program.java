package com.my.power.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by cool04ek on 15.12.13.
 */
@DatabaseTable(tableName = "programs_table")
public class Program extends BaseDbModel implements Serializable {

    static final long serialVersionUID = 1L;

    @DatabaseField(id = true)
    private String id;
    @DatabaseField
    private String name;
    @DatabaseField
    private String author;
    @DatabaseField
    private String description;
    @DatabaseField
    private String image;
    @ForeignCollectionField(eager = true)
    private ForeignCollection<Week> weekForeignCollection;
    @DatabaseField
    private boolean inProgress;
    @DatabaseField(dataType = DataType.DATE_STRING, format = "dd.MM.yyyy")
    private Date startDate;

//    private ArrayList<Week> weeks;

    public Program() {
    }

    public Program(String name, String image, String description) {
        this.name = name;
        this.image = image;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

//    public ArrayList<Week> getWeeks() {
//        return weeks;
//    }
//
//    public void setWeeks(ArrayList<Week> weeks) {
//        this.weeks = weeks;
//    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ForeignCollection<Week> getWeekForeignCollection() {
        return weekForeignCollection;
    }

    public void setWeekForeignCollection(ForeignCollection<Week> weekForeignCollection) {
        this.weekForeignCollection = weekForeignCollection;
    }

    public boolean isInProgress() {
        return inProgress;
    }

    public void setInProgress(boolean inProgress) {
        this.inProgress = inProgress;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Program)) return false;

        Program program = (Program) o;

        if (id != null ? !id.equals(program.id) : program.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
