package com.my.power.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.my.power.R;


/**
 * Created by cool04ek on 10.12.13.
 */
public class BaseFragment extends Fragment implements View.OnClickListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_start, null);
    }

    @Override
    public void onClick(View view) {

    }

    protected void showUnimplementedToast() {
        Toast.makeText(getActivity(), getString(R.string.toast_unimplemeted), Toast.LENGTH_SHORT).show();
    }
}
