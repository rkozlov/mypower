package com.my.power.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.my.power.R;
import com.my.power.activity.MainActivity;
import com.my.power.services.UserService;


/**
 * Created by cool04ek on 10.12.13.
 */
public class StartFragment extends BaseFragment {

    private LinearLayout mLinearLayoutTitle;
    private Button mButtonMain;
    private Button mButtonIndicators;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_start, null);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mButtonMain = (Button) getView().findViewById(R.id.fragment_start_main_btn);
        mButtonIndicators = (Button) getView().findViewById(R.id.fragment_start_indicators_btn);
        mLinearLayoutTitle = (LinearLayout) getView().findViewById(R.id.fragment_start_title_layout);

        mLinearLayoutTitle.setOnClickListener(this);
        mButtonMain.setOnClickListener(this);
        mButtonIndicators.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Intent intent = null;
        switch (view.getId()) {
            case R.id.fragment_start_main_btn:
                intent = new Intent(getActivity(), MainActivity.class);
                getActivity().startActivity(intent);
                break;
            case R.id.fragment_start_indicators_btn:
                UserService.getInstance().showParamsAlert(getActivity(), null);
                break;
            case R.id.fragment_start_title_layout:
                break;
        }
    }
}
