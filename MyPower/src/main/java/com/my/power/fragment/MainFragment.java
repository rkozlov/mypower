package com.my.power.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.my.power.R;
import com.my.power.db.dao.ProgramTableDao;
import com.my.power.model.Program;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;


/**
 * Created by cool04ek on 10.12.13.
 */

/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends BaseFragment {

    private ArrayList<Program> mListPrograms;
    private ArrayList<Program> mListSearchPrograms;
    private ListView mListViewPrograms;
    private ProgramsAdapter mAdapter;
    private DisplayImageOptions mDefaultDisplayImageOptions;

    private ProgramTableDao mProgramTableDao;

    public MainFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mListViewPrograms = (ListView) getView().findViewById(R.id.fragment_main_listview);

        mDefaultDisplayImageOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .showImageOnFail(R.drawable.logo_more)
                .showImageForEmptyUri(R.drawable.logo_more)
                .showStubImage(R.drawable.logo_more)
                .delayBeforeLoading(100)
                .build();

        mListSearchPrograms = new ArrayList<Program>();

        mAdapter = new ProgramsAdapter();
        mListViewPrograms.setAdapter(mAdapter);

        mProgramTableDao = new ProgramTableDao(getActivity());

        mListPrograms = (ArrayList<Program>) mProgramTableDao.getAll();
        if (mListPrograms.size() > 0) {
            mListSearchPrograms = new ArrayList<Program>(mListPrograms);
            mAdapter.notifyDataSetChanged();
        }

        mListViewPrograms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                getFragmentManager().beginTransaction()
//                        .replace(R.id.container, ProgramInfoFragment.newInstance(response.getProgram()))
                        .replace(R.id.container, new ProgramInfoFragment().newInstance(mListSearchPrograms.get(i)))
                        .addToBackStack(null)
                        .commit();
                //TODO:
            }
        });
    }

    private class ProgramsAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mListSearchPrograms.size();
        }

        @Override
        public Object getItem(int i) {
            return mListSearchPrograms.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = View.inflate(getActivity(), R.layout.cell_program, null);
                holder = new ViewHolder();

                holder.imageViewPicture = (ImageView) convertView.findViewById(R.id.cell_program_img);
                holder.textViewAuthor = (TextView) convertView.findViewById(R.id.cell_program_author);
                holder.textViewName = (TextView) convertView.findViewById(R.id.cell_program_name);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.textViewName.setText(mListSearchPrograms.get(position).getName());
            holder.textViewAuthor.setText(mListSearchPrograms.get(position).getAuthor());
            ImageLoader.getInstance().displayImage(mListSearchPrograms.get(position).getImage(),
                    holder.imageViewPicture, mDefaultDisplayImageOptions);

            return convertView;
        }

        protected class ViewHolder {
            private ImageView imageViewPicture;
            private TextView textViewName;
            private TextView textViewAuthor;
        }
    }

    public void searchProgram(String key) {
        mListSearchPrograms.clear();
        key = key.toLowerCase();
        for (Program program : mListPrograms) {
            if (program.getName().toLowerCase().contains(key) || program.getDescription().toLowerCase().contains(key)) {
                mListSearchPrograms.add(program);
            }
        }
        mAdapter.notifyDataSetChanged();
    }
}
