package com.my.power.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.my.power.R;
import com.my.power.activity.MainActivity;
import com.my.power.services.UserService;


/**
 * Created by cool04ek on 10.12.13.
 */
public class AboutFragment extends BaseFragment {

        @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, null);
    }

}
