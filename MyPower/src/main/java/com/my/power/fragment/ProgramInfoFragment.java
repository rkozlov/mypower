package com.my.power.fragment;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;

import com.my.power.R;
import com.my.power.db.dao.ExerciseTableDao;
import com.my.power.db.dao.ProgramTableDao;
import com.my.power.model.BaseProgramInfo;
import com.my.power.model.Day;
import com.my.power.model.Exercise;
import com.my.power.model.Program;
import com.my.power.model.User;
import com.my.power.model.Week;
import com.my.power.services.UserService;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


/**
 * Created by cool04ek on 10.12.13.
 */

public class ProgramInfoFragment extends BaseFragment {

    private static final String KEY_EXTRA_PROGRAM = "key_extra_program";

    private TextView mTextViewTitle;
    private TextView mTextViewAuthor;
    private TextView mTextViewDescription;
    private ImageView mImageViewPicture;
    private Button mButtonStartProgram;

    private LinearLayout mProgramInfoHeader;
    private ListView mListViewProgramInfo;
    private ProgramInfoAdapter mAdapter;

    private Program mProgram;

    private ArrayList<BaseProgramInfo> mArrayListProgramInfos;

    //----------------------

    private ArrayList<Day> mListDays;

    private TableRow mTableRowGreat;
    private TableRow mTableRowNormal;
    private TableRow mTableRowBad;
    private TableRow mTableRowLegend;

    private TextView mTextViewStartDate;
    private TextView mTextViewMaxPress;

    //----------------------

    private ProgramTableDao mProgramTableDao;
    private ExerciseTableDao mExerciseTableDao;

    private UserService mUserService = UserService.getInstance();

    public ProgramInfoFragment() {
    }

    public static ProgramInfoFragment newInstance(Program program) {
        ProgramInfoFragment fragment = new ProgramInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_EXTRA_PROGRAM, program);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_program_info, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DisplayImageOptions defaultDisplayImageOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .showImageOnFail(R.drawable.logo_more)
                .showImageForEmptyUri(R.drawable.logo_more)
                .showStubImage(R.drawable.logo_more)
//                .resetViewBeforeLoading(true)
                .build();

        mProgramInfoHeader = (LinearLayout) View.inflate(getActivity(), R.layout.header_program_info_list, null);
        mListViewProgramInfo = (ListView) getView().findViewById(R.id.fragment_program_info_list);

        mTextViewTitle = (TextView) mProgramInfoHeader.findViewById(R.id.fragment_program_title);
        mTextViewAuthor = (TextView) mProgramInfoHeader.findViewById(R.id.fragment_program_author);
        mTextViewDescription = (TextView) mProgramInfoHeader.findViewById(R.id.fragment_program_description);
        mImageViewPicture = (ImageView) mProgramInfoHeader.findViewById(R.id.fragment_program_image);
        mButtonStartProgram = (Button) mProgramInfoHeader.findViewById(R.id.fragment_program_start);

        mTableRowGreat = (TableRow) mProgramInfoHeader.findViewById(R.id.fragment_program_great);
        mTableRowNormal = (TableRow) mProgramInfoHeader.findViewById(R.id.fragment_program_normal);
        mTableRowBad = (TableRow) mProgramInfoHeader.findViewById(R.id.fragment_program_bad);
        mTableRowLegend = (TableRow) mProgramInfoHeader.findViewById(R.id.fragment_program_legend);
        mTextViewStartDate = (TextView) mProgramInfoHeader.findViewById(R.id.fragment_program_date);
        mTextViewMaxPress = (TextView) mProgramInfoHeader.findViewById(R.id.fragment_program_max_press);

        mButtonStartProgram.setOnClickListener(this);

        mProgramTableDao = new ProgramTableDao(getActivity());
        mExerciseTableDao = new ExerciseTableDao(getActivity());

        Bundle args = getArguments();
        if (args != null) {
            mProgram = (Program) args.getSerializable(KEY_EXTRA_PROGRAM);
        }

        if (mProgram != null) {
            mTextViewTitle.setText(mProgram.getName());
            mTextViewAuthor.setText(mProgram.getAuthor());
            mTextViewDescription.setText(mProgram.getDescription());

            initProgramInfoList();

            if (mProgram.isInProgress()) {
                initResultTable();
            }

            mListViewProgramInfo.addHeaderView(mProgramInfoHeader);
            mAdapter = new ProgramInfoAdapter();
            mListViewProgramInfo.setAdapter(mAdapter);

            ImageLoader.getInstance().displayImage(mProgram.getImage(),
                    mImageViewPicture, defaultDisplayImageOptions);

            setStartProgramButtonState();
        } else {
            //TODO:
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.fragment_program_start:
                startProgramIfPossible();
                break;
        }
    }

    private void initProgramInfoList() {
        mArrayListProgramInfos = new ArrayList<BaseProgramInfo>();
        mListDays = new ArrayList<Day>();
        for (Week week : mProgram.getWeekForeignCollection()) {
            mArrayListProgramInfos.add(week);
            for (Day day : week.getDayForeignCollection()) {
                mListDays.add(day);
                mArrayListProgramInfos.add(day);
                for (Exercise exercise : day.getExerciseForeignCollection()) {
                    mArrayListProgramInfos.add(exercise);
                }
            }
        }
    }

    private void setStartProgramButtonState() {
        mButtonStartProgram.setSelected(mProgram.isInProgress());
        if (mProgram.isInProgress()) {
            mButtonStartProgram.setText(getString(R.string.program_info_in_progress));
        } else {
            mButtonStartProgram.setText(getString(R.string.program_info_start));
        }
    }

    private void startProgramIfPossible() {
        if (mProgram.isInProgress()) {
            return;
        }
        Program programInProgress = mUserService.getUser().getProgram();
        if (programInProgress == null) {
            mProgram.setInProgress(true);
            mProgram.setStartDate(new Date());
            mProgramTableDao.update(mProgram);
            setStartProgramButtonState();
            mUserService.getUser().setProgram(mProgram);

            initResultTable();
        } else {
            showStopOtherProgramAlert(programInProgress);
        }
    }

    private void showStopOtherProgramAlert(final Program program) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.program_info_program_in_progress, program.getName()));
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                program.setInProgress(false);
                mProgram.setInProgress(true);
                stopOtherProgram(program);
                mProgramTableDao.update(program);
                mProgramTableDao.update(mProgram);
                setStartProgramButtonState();
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    private void showStartProgramAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.program_info_start_program));
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startProgramIfPossible();
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    private void stopOtherProgram(Program program) {
        for (Week week : program.getWeekForeignCollection()) {
            for (Day day : week.getDayForeignCollection()) {
                for (Exercise exercise : day.getExerciseForeignCollection()) {
                    if (exercise.isDone()) {
                        exercise.setDone(false);
                        mExerciseTableDao.update(exercise);
                    }
                }
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.FROYO)
    public void searchItem(String query) {
        query = query.toLowerCase();
        for (int i = 0; i < mAdapter.getCount(); i++) {
            if (mAdapter.getItem(i).getName().toLowerCase().contains(query)) {
                mListViewProgramInfo.smoothScrollToPosition(i);
                return;
            }
        }
    }

    public void notifyDataSetChanged() {
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    public class ProgramInfoAdapter extends BaseAdapter {

        private static final int TYPE_WEEK = 0;
        private static final int TYPE_DAY = 1;
        private static final int TYPE_EXERCISE = 2;

        private static final int TYPES_COUNT = 3;

        private int benchPress = mUserService.getUser().getBenchPress();

        @Override
        public int getCount() {
            return mArrayListProgramInfos.size();
        }

        @Override
        public int getViewTypeCount() {
            return TYPES_COUNT;
        }

        @Override
        public int getItemViewType(int position) {
            if (mArrayListProgramInfos.get(position) instanceof Week) {
                return TYPE_WEEK;
            } else if (mArrayListProgramInfos.get(position) instanceof Day) {
                return TYPE_DAY;
            } else if (mArrayListProgramInfos.get(position) instanceof Exercise) {
                return TYPE_EXERCISE;
            } else {
                return -1;
            }
        }

        @Override
        public BaseProgramInfo getItem(int position) {
            return mArrayListProgramInfos.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            switch (getItemViewType(position)) {
                case TYPE_WEEK:
                    ViewHolderWeek holderWeek;
                    if (convertView == null) {
                        convertView = View.inflate(getActivity(), R.layout.cell_program_info_week, null);

                        holderWeek = new ViewHolderWeek();
                        holderWeek.textViewName = (TextView) convertView.findViewById(R.id.cell_program_info_week_name);

                        convertView.setTag(holderWeek);
                    } else {
                        holderWeek = (ViewHolderWeek) convertView.getTag();
                    }

                    Week week = (Week) getItem(position);
                    holderWeek.textViewName.setText(week.getName());

                    return convertView;
                case TYPE_DAY:
                    ViewHolderDay holderDay;
                    if (convertView == null) {
                        convertView = View.inflate(getActivity(), R.layout.cell_program_info_day, null);

                        holderDay = new ViewHolderDay();
                        holderDay.textViewName = (TextView) convertView.findViewById(R.id.cell_program_info_day_name);

                        convertView.setTag(holderDay);
                    } else {
                        holderDay = (ViewHolderDay) convertView.getTag();
                    }

                    Day day = (Day) getItem(position);
                    holderDay.textViewName.setText(day.getName());

                    return convertView;
                case TYPE_EXERCISE:
                    ViewHolderExercise holderExercise;
                    if (convertView == null) {
                        convertView = View.inflate(getActivity(), R.layout.cell_program_info_exercise, null);

                        holderExercise = new ViewHolderExercise();
                        holderExercise.textViewName = (TextView) convertView.findViewById(R.id.cell_program_info_exercise_name);
                        holderExercise.textViewDescription = (TextView) convertView.findViewById(R.id.cell_program_info_exercise_description);
                        holderExercise.imageButtonDone = (ImageButton) convertView.findViewById(R.id.cell_program_info_exercise_done);

                        convertView.setTag(holderExercise);
                    } else {
                        holderExercise = (ViewHolderExercise) convertView.getTag();
                    }

                    final Exercise exercise = (Exercise) getItem(position);
                    holderExercise.textViewName.setText(exercise.getName());

                    holderExercise.textViewDescription.setText(getString(R.string.program_info_description,
                            exercise.getIterations(), exercise.getTimes(), benchPress == User.NO_PRESS ? exercise.getWeight() : exercise.getWeight() * benchPress));

                    holderExercise.imageButtonDone.setSelected(exercise.isDone());
                    holderExercise.imageButtonDone.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mProgram.isInProgress()) {
                                boolean done = v.isSelected();
                                v.setSelected(!done);
                                exercise.setDone(!done);
                                mExerciseTableDao.update(exercise);
                            } else {
                                showStartProgramAlert();
                            }
                        }
                    });
                    return convertView;
                default:
                    return convertView;
            }
        }

        protected class ViewHolderWeek {
            protected TextView textViewName;
        }

        protected class ViewHolderDay {
            protected TextView textViewName;
        }

        protected class ViewHolderExercise {
            protected TextView textViewName;
            protected TextView textViewDescription;
            protected ImageButton imageButtonDone;
        }

        @Override
        public void notifyDataSetChanged() {
            benchPress = mUserService.getUser().getBenchPress();
            super.notifyDataSetChanged();
        }
    }

    private void initResultTable() {
        ImageView.ScaleType scaleType = ImageView.ScaleType.CENTER_INSIDE;
        for (Day day: mListDays) {
            TableRow.LayoutParams params = new TableRow.LayoutParams(getActivity().getResources().getDimensionPixelSize(R.dimen.program_info_table_cell_width),
                    getActivity().getResources().getDimensionPixelSize(R.dimen.program_info_table_cell_height));
            params.weight = 1;
            ImageView imageViewGreat = new ImageView(getActivity());
            imageViewGreat.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_rect));
            imageViewGreat.setLayoutParams(params);
            imageViewGreat.setScaleType(scaleType);
            imageViewGreat.setPadding(0, 20, 0, 20);
            mTableRowGreat.addView(imageViewGreat);

            ImageView imageViewNormal = new ImageView(getActivity());
            imageViewNormal.setLayoutParams(params);
            imageViewNormal.setPadding(0, 20, 0, 20);
            imageViewNormal.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_rect));
            imageViewNormal.setScaleType(scaleType);
            mTableRowNormal.addView(imageViewNormal);

            ImageView imageViewBad = new ImageView(getActivity());
            imageViewBad.setLayoutParams(params);
            imageViewBad.setPadding(0, 20, 0, 20);
            imageViewBad.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_rect));
            imageViewBad.setScaleType(scaleType);
            mTableRowBad.addView(imageViewBad);

            TextView textViewLegend = new TextView(getActivity());
            textViewLegend.setLayoutParams(params);
            mTableRowLegend.addView(textViewLegend);

            int doneExercises = 0;
            for (Exercise exercise: day.getExerciseForeignCollection()) {
                if (exercise.isDone()) {
                    doneExercises++;
                }
            }
            int allExercises = day.getExerciseForeignCollection().size();
            double percentage = (double) doneExercises / allExercises;
            if (percentage == 1) {
                imageViewGreat.setImageResource(R.drawable.red);
            } else if (percentage >= 0.5) {
                imageViewNormal.setImageResource(R.drawable.blue);
            } else {
                imageViewBad.setImageResource(R.drawable.green);
            }

            textViewLegend.setText(getString(R.string.program_info_fragment_legend_pattern, day.getWeek().getName(), day.getName()));
        }
    }
}