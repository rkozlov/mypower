package com.my.power.activity;

import android.os.Bundle;

import com.my.power.R;
import com.my.power.fragment.StartFragment;

public class StartActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        getSupportActionBar().hide();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_base_container, new StartFragment()).commit();
    }
}
