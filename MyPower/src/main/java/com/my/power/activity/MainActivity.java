package com.my.power.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.my.power.R;
import com.my.power.fragment.AboutFragment;
import com.my.power.fragment.MainFragment;
import com.my.power.fragment.ProgramInfoFragment;
import com.my.power.network.WebService;
import com.my.power.services.UserService;

public class MainActivity extends ActionBarActivity implements SearchView.OnQueryTextListener {

    private MainFragment mMainFragment;
    private ProgramInfoFragment mProgramInfoFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            mMainFragment = new MainFragment();
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, mMainFragment)
                    .commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        WebService.getInstance().setActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        WebService.getInstance().setActivity(null);
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);

        if (fragment instanceof ProgramInfoFragment) {
            mProgramInfoFragment = (ProgramInfoFragment) fragment;
        } else {
            mProgramInfoFragment = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        if (searchView != null) {
            searchView.setQueryHint(getString(R.string.action_search));
            searchView.setOnQueryTextListener(this);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_about:
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, new AboutFragment())
                        .addToBackStack(null)
                        .commit();
                return true;
            case R.id.action_indicators:
                UserService.getInstance().showParamsAlert(this, mUserInfoListener);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        if (mProgramInfoFragment != null) {
            mProgramInfoFragment.searchItem(s);
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        if (mMainFragment != null) {
            mMainFragment.searchProgram(s);
        }
        return false;
    }

    protected void showUnimplementedToast() {
        Toast.makeText(this, getString(R.string.toast_unimplemeted), Toast.LENGTH_SHORT).show();
    }

    private UserService.UserInfoListener mUserInfoListener = new UserService.UserInfoListener() {

        @Override
        public void onUserParamsChanged() {
            if (mProgramInfoFragment != null) {
                mProgramInfoFragment.notifyDataSetChanged();
            }
        }
    };
}
