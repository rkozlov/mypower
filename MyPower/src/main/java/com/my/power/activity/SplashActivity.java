package com.my.power.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.my.power.R;
import com.my.power.db.dao.DayTableDao;
import com.my.power.db.dao.ProgramTableDao;
import com.my.power.db.dao.WeekTableDao;
import com.my.power.model.Day;
import com.my.power.model.Program;
import com.my.power.model.Week;
import com.my.power.network.ResponseReceiver;
import com.my.power.network.WebService;
import com.my.power.network.response.AllProgramsResponse;
import com.my.power.network.response.ProgramResponse;

import java.util.ArrayList;

public class SplashActivity extends BaseActivity {

    private ArrayList<Program> mListPrograms;

    private ProgramTableDao mProgramTableDao;
    private WeekTableDao mWeekTableDao;
    private DayTableDao mDayTableDao;

    private int threadsCount = 0;
    private int threadsFinishedCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();

        mProgramTableDao = new ProgramTableDao(this);
        mWeekTableDao = new WeekTableDao(this);
        mDayTableDao = new DayTableDao(this);

        Toast.makeText(this, getString(R.string.loading), Toast.LENGTH_LONG).show();
        mListPrograms = (ArrayList<Program>) mProgramTableDao.getAll();
        if (mListPrograms == null) {
            mListPrograms = new ArrayList<Program>();
        }
        fetchPrograms();
    }

    private void fetchPrograms() {
        WebService.getInstance().requestAllPrograms(new ResponseReceiver<AllProgramsResponse>() {

            @Override
            public void onSuccess(AllProgramsResponse response) {
                if (response.getCode() == WebService.CODE_OK) {
                    new SaveProgramsAsyncTask().execute(response);
                }
            }

            @Override
            public void onFailure() {
                super.onFailure();

                if (mListPrograms.size() > 0) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            navigateNext();
                        }
                    }, 3000);
                } else {
                   showNoInternetAlert();
                }
            }
        });
    }

    private void checkProgramsInfo() {
        for (Program program : mListPrograms) {
            if (program.getWeekForeignCollection() == null) {
                final String id = program.getId();
                WebService.getInstance().requestProgram(id, new ResponseReceiver<ProgramResponse>() {
                    @Override
                    public void onSuccess(ProgramResponse response) {
                        if (response.getCode() == WebService.CODE_OK) {
                            threadsCount++;
                            new SaveProgramInfoAsyncTask().execute(response);
                        }
                    }
                });
            }
        }
    }

    private void navigateNext() {
        Intent intent = new Intent(SplashActivity.this, StartActivity.class);
        startActivity(intent);
        SplashActivity.this.finish();
    }

    private void showNoInternetAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setMessage(getString(R.string.no_connection));
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SplashActivity.this.finish();
            }
        });

        builder.create().show();
    }

    private class SaveProgramsAsyncTask extends AsyncTask<AllProgramsResponse, Void, Boolean> {

        @Override
        protected Boolean doInBackground(AllProgramsResponse... params) {
            boolean newProgram = false;

            for (Program program : params[0].getPrograms()) {
                if (!mListPrograms.contains(program)) {
                    mProgramTableDao.create(program);
                    mListPrograms.add(program);
                    newProgram = true;
                }
            }

            return newProgram;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) {
                checkProgramsInfo();
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        navigateNext();
                    }
                }, 3000);
            }
        }
    }

    private class SaveProgramInfoAsyncTask extends AsyncTask<ProgramResponse, Void, Boolean> {

        @Override
        protected Boolean doInBackground(ProgramResponse... params) {
            Program program = params[0].getProgram();
            int index = mListPrograms.indexOf(program);
            if (index >= 0) {
                mProgramTableDao.refresh(program);
                for (Week week : program.getWeekForeignCollection()) {
                    mWeekTableDao.refresh(week);
                    for (Day day : week.getDayForeignCollection()) {
                        mDayTableDao.refresh(day);
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (threadsCount == ++threadsFinishedCount) {
                navigateNext();
            }
        }
    }
}
