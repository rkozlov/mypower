package com.my.power.activity;

import android.support.v7.app.ActionBarActivity;

import com.my.power.network.WebService;

/**
 * Created by user on 11.05.14.
 */
public class BaseActivity extends ActionBarActivity {

    @Override
    protected void onResume() {
        super.onResume();
        WebService.getInstance().setActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        WebService.getInstance().setActivity(null);
    }
}
