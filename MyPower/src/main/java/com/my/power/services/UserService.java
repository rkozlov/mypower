package com.my.power.services;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.my.power.MyPowerApplication;
import com.my.power.R;
import com.my.power.model.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

/**
 * Created by user on 12.06.14.
 */
public class UserService {

    public interface UserInfoListener {
        void onUserParamsChanged();
    }

    private final static String FILE_NAME = "user_file";

    private User user;
    private Context context = MyPowerApplication.getAppContext();

    private static UserService ourInstance = new UserService();

    public static UserService getInstance() {
        return ourInstance;
    }

    private UserService() {
    }

    public User getUser() {
        if (user == null) {
            user = getSerializedUser();
        }
        if (user == null) {
            initUser();
        }
        return user;
    }

    public void setUser(User user) {
        this.user = user;
        serializeUser(user);
    }

    private void serializeUser(User user) {
        if (context == null) {
            return;
        }
        FileOutputStream fos;
        ObjectOutputStream os;
        try {
            fos = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            os = new ObjectOutputStream(fos);
            os.writeObject(user);
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private User getSerializedUser() {
        if (context == null) {
            return null;
        }
        FileInputStream fis;
        ObjectInputStream is;
        try {
            fis = context.openFileInput(FILE_NAME);
            is = new ObjectInputStream(fis);
            User object = (User) is.readObject();
            is.close();
            return object;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void initUser() {
        user = new User();
    }

    public void showParamsAlert(Activity activity, final UserInfoListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        View view = View.inflate(context, R.layout.alert_parameters, null);
        final EditText editTextWeight = (EditText) view.findViewById(R.id.alert_params_weight);
        final EditText editTextPower = (EditText) view.findViewById(R.id.alert_params_power);
        Button buttonOk = (Button) view.findViewById(R.id.alert_params_ok);
        Button buttonCancel = (Button) view.findViewById(R.id.alert_params_cancel);

        if (getUser().getWeight() != User.NO_WEIGHT) {
            editTextWeight.setText(String.valueOf(getUser().getWeight()));
        }
        if (getUser().getBenchPress() != User.NO_PRESS) {
            editTextPower.setText(String.valueOf(getUser().getBenchPress()));
        }

        builder.setView(view);

        final AlertDialog dialog = builder.create();
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String weight = editTextWeight.getText().toString();
                if (!weight.equals("")) {
                    user.setWeight(Float.valueOf(weight));
                }
                String press = editTextPower.getText().toString();
                if (!press.equals("")) {
                    user.setBenchPress(Integer.valueOf(press));
                    if (listener != null) {
                        listener.onUserParamsChanged();
                    }
                }
                serializeUser(user);
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
